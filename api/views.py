from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAdminUser, DjangoModelPermissionsOrAnonReadOnly
from rest_framework.response import Response
from rest_framework.status import HTTP_401_UNAUTHORIZED
from rest_framework.viewsets import ModelViewSet

from api.serializers import BookSerializer, BookTypesSerializer, JournalSerializer
from main.models import Book, Client, Journal, BookType


@api_view(["POST"])
def login(request):
    username = request.data.get("username")
    password = request.data.get("password")

    user = authenticate(username=username, password=password)
    if not user:
        return Response({"error": "Login failed"}, status=HTTP_401_UNAUTHORIZED)

    token, _ = Token.objects.get_or_create(user=user)
    return Response({"token": token.key})


class BookAPIView(ModelViewSet):
    permission_classes = (DjangoModelPermissionsOrAnonReadOnly,)
    serializer_class = BookSerializer
    queryset = Book.objects.all()


class ClientAPIView(ModelViewSet):
    permission_classes = (DjangoModelPermissionsOrAnonReadOnly,)
    queryset = Client.objects.all()
    serializer_class = JournalSerializer


class BookTypesAPIView(ModelViewSet):
    permission_classes = (DjangoModelPermissionsOrAnonReadOnly,)
    queryset = BookType.objects.all()
    serializer_class = BookTypesSerializer


class JournalAPIView(ModelViewSet):
    permission_classes = (IsAdminUser,)
    queryset = Journal.objects.all()
    serializer_class = JournalSerializer
