from main.models import Book
from django.views.generic import TemplateView

# Create your views here.

class Index():
    pass


class JournalView(TemplateView):

    template_name = 'adm/journal.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['books'] = Book.objects.all()
        context['jopa'] = 'Жопа'
        return context
