"""library URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from api import views as api_views
from main import views as main_views
from rest_framework.routers import SimpleRouter
from adm import views as adm_views
from django.views.generic import TemplateView
from main.models import Book

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/login', api_views.login),

    path('manage', TemplateView.as_view(template_name='adm/index.html')),
    path('manage/journal', adm_views.JournalView.as_view(), name='journal')
]

router = SimpleRouter()
router.register("api/books", api_views.BookAPIView)
router.register("api/book_typess", api_views.BookTypesAPIView)
router.register("api/clients", api_views.ClientAPIView)
router.register("api/journal", api_views.JournalAPIView, basename='api_journal')
urlpatterns += router.urls
