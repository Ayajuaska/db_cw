# from django.shortcuts import render
from django.views.generic import ListView
from main.models import Book


class BooksIndex(ListView):
    model = Book

