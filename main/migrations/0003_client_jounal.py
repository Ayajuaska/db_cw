# Generated by Django 2.1.1 on 2018-10-20 10:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20180924_2139'),
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.TextField()),
                ('last_name', models.TextField()),
                ('mid_name', models.TextField()),
                ('pass_prefix', models.IntegerField()),
                ('pass_number', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Jounal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_reg', models.DateField(auto_now=True)),
                ('date_end', models.DateField(auto_now=True)),
                ('date_ret', models.DateField(auto_now=True)),
                ('book', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Book')),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Client')),
            ],
        ),
    ]
