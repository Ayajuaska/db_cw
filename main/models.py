import json
from django.db import models


class BookType(models.Model):
    """
    Class representing book types table
    """
    name = models.TextField()
    cnt = models.IntegerField()
    fine = models.IntegerField()
    day_count = models.IntegerField()

    def as_dict(self):
        return {
            'name': self.name,
            'cnt': self.cnt,
            'fine': self.fine,
            'day_count': self.day_count
        }


class Book(models.Model):
    """
    Class representing books table
    """
    name = models.TextField()
    cnt = models.IntegerField()
    type = models.ForeignKey(to=BookType, on_delete=models.CASCADE)

    def as_dict(self) -> dict:
        return {
            'name': self.name,
            'cnt': self.cnt,
            'type': self.type.name
        }


class Client(models.Model):
    first_name = models.TextField()
    last_name = models.TextField()
    mid_name = models.TextField()
    pass_prefix = models.IntegerField()
    pass_number = models.IntegerField()

    def as_dict(self) -> dict:
        return {
            'first_name': self.first_name,
            'last_name': self.last_name,
            'mid_name': self.mid_name,
            'pass_prefix': self.pass_prefix,
            'pass_number': self.pass_number,
        }


class Journal(models.Model):
    book = models.ForeignKey(to=Book, on_delete=models.CASCADE)
    client = models.ForeignKey(to=Client, on_delete=models.CASCADE)
    date_reg = models.DateField(auto_now=True)
    date_end = models.DateField(auto_now=True)
    date_ret = models.DateField(auto_now=True)

    def as_dict(self) -> dict:
        return {
            'book': self.book.as_dict(),
            'client': self.client.as_dict(),
            'date_reg': self.date_reg,
            'date_end': self.date_end,
            'date_ret': self.date_ret,
        }
